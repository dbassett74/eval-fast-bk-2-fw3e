using FortCodeExercises.Exercise1.Enums;
using System;

namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        private MachineType _type;
        private bool _hasMaxSpeed = true;
        private int _maxSpeed = 70;
        private Color _baseColor = Color.White;
        private Color? _trimColor;
        private bool _isDark = false;

        public string Name { get { return _type.ToString(); } }
        public string Description { get { return $"{_baseColor} {Name} [{_maxSpeed}]."; } }
        public int MaxSpeed { get { return _maxSpeed; } }
        public Color Color { get { return _baseColor; } }
        public Color? TrimColor { get { return _trimColor; } }
        public MachineType Type { get { return _type; } }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type">The type of machine</param>
        public Machine(MachineType type)
        {
            try
            {
                _type = type;

                SetBaseColor();
                SetTrimColor();
                SetHasMaxSpeed();
                SetMaxSpeed();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Set the base color by it's type
        /// </summary>
        private void SetBaseColor()
        {
            switch (_type)
            {
                case MachineType.Bulldozer:
                    _baseColor = Color.Red;
                    break;
                case MachineType.Crane:
                    _baseColor = Color.Blue;
                    break;
                case MachineType.Tractor:
                    _baseColor = Color.Green;
                    break;
                case MachineType.Truck:
                    _baseColor = Color.Yellow;
                    break;
                case MachineType.Car:
                    _baseColor = Color.Brown;
                    break;
            }

            // Set IsDark
            switch (_baseColor)
            {
                case Color.Red:
                case Color.Green:
                case Color.Black:
                case Color.Crimson:
                    _isDark = true;
                    break;
            }
        }

        /// <summary>
        /// Set the trim color by it's base color
        /// </summary>
        private void SetTrimColor()
        {
            switch (_type)
            {
                case MachineType.Crane:
                    if (_isDark)
                        _trimColor = Color.Black;
                    else
                        _trimColor = Color.White;
                    break;
                case MachineType.Tractor:
                    _trimColor = Color.Gold;
                    break;
                case MachineType.Truck:
                    _trimColor = Color.Silver;
                    break;
            }
        }

        /// <summary>
        /// Set if the machine has a max speed by it's type
        /// </summary>
        private void SetHasMaxSpeed()
        {
            switch (_type)
            {
                case MachineType.Truck:
                case MachineType.Car:
                    _hasMaxSpeed = false;
                    break;
                default:
                    _hasMaxSpeed = true;
                    break;
            }
        }

        /// <summary>
        /// Set the max speed based on type
        /// </summary>
        private void SetMaxSpeed()
        {
            // Set max speed
            switch (_type)
            {
                case MachineType.Bulldozer:
                    _maxSpeed = 80;
                    break;
                case MachineType.Crane:
                    _maxSpeed = 75;
                    break;
                case MachineType.Tractor:
                    _maxSpeed = 90;
                    break;
            }
        }
    }
}
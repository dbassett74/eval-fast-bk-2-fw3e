﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1.Enums
{
    public enum MachineType
    {
        Bulldozer,
        Crane,
        Tractor,
        Truck,
        Car
    }

    public enum Color
    {
        White,
        Red,
        Blue,
        Green,
        Yellow,
        Brown,
        Gold,
        Silver,
        Black,
        Beige,
        BabyBlue,
        Crimson
    }
}

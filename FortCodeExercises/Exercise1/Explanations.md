# FORT Technical Exercise

Added Enum.cs

Refectored string values for machine types into an Enumeration contained in Enum.cs

Refactored string values for colors (base and trim) into an Enumeration contained in Enum.cs

Capitalized all property names in keeping with standard C# convention

Added private field variables to hold property values set during initialization rather than using logic each time it's referenced

Added Constructor which requires machine type for initialization of object

Extracted and refactored hasMaxSpeed code from the Description getter into it's own method

Extracted and refactored logic from trimColor getter to it's own method

Refactored and removed redundant if statements into cleaner switch statements

Added test client
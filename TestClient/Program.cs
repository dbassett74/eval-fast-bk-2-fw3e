﻿using FortCodeExercises.Exercise1;
using FortCodeExercises.Exercise1.Enums;
using System;
using static FortCodeExercises.Exercise1.Machine;

namespace TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var machine = new Machine(MachineType.Crane);
            Console.WriteLine($"Name: {machine.Name}");
            Console.WriteLine($"Type: {machine.Type}");
            Console.WriteLine($"Description: {machine.Description}");
            Console.WriteLine($"Color: {machine.Color}");
            Console.WriteLine($"Trim Color: {machine.TrimColor}");
        }
    }
}
